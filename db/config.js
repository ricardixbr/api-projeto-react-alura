var config = {
  database: {
    connectionString: "mongodb://localhost:27017/dev",
    databaseName: "schema-dev"
  },
  debug: {
    database: {
      connectionString: "mongodb://localhost:27017/test",
      databaseName: "schema-test"
    }
  }
};

module.exports = config;