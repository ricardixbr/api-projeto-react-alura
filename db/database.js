const config = require("./config");
const mongoose = require("mongoose");

const connectionString = process.env.debug === "true" ?
  config.debug.database.connectionString :
  config.database.connectionString;

mongoose.connect(connectionString);

mongoose.connection.on("connected", function() {
  console.log("Conectado em " + connectionString);
});

mongoose.connection.on("error", function(error) {
  console.log("Conectado em " + connectionString + " falhou:" + error);
});

mongoose.connection.on("disconnected", function() {
  console.log("Desconectado de " + connectionString);
});

process.on("SIGINT", function() {
  mongoose.connection.close(function() {
    console.log("Desconectado de " + connectionString + " através do término da aplicação");
    process.exit(0);
  });
});