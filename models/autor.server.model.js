'use strict';

const mongoose = require('mongoose');

const AutorSchema = new mongoose.Schema({
  nome: {
    type: String,
    trim: true,
    required: 'Autor cannot be blank'
  },
  email: {
    type: String,
    trim: true,
    required: 'Email cannot be blank',
    lowercase: true
  },
  senha: {
    type: String,
    trim: true,
    required: 'Senha cannot be blank',
    lowercase: true
  }
});

module.exports = mongoose.model('Autor', AutorSchema);