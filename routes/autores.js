'use strict';

require("../db/database");

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Autor = require('../models/autor.server.model');
const router = express.Router();

router.route('/')

  // create autor (POST http://localhost:3001/autores)
  .post(function(req, res) {
    let autor = new Autor();
    autor.nome = req.body.nome;
    autor.email = req.body.email;
    autor.senha = req.body.senha;

    autor.save(function(err) {
      if (err)
        res.send(err);
      
        Autor.find(function(err, autores) {
            if (err)
                res.send(err);

            res.json(autores);
        });

    });  
  })

  // get all (GET http://localhost:3001/autores)
  .get(function(req, res) {
      Autor.find(function(err, autores) {
          if (err)
              res.send(err);

          res.json(autores);
      });
  });


// routes /autores/:autor_id
// ----------------------------------------------------
router.route('/:autor_id')

// get the autor with that id (GET http://localhost:3001/autores/:autor_id)
.get(function(req, res) {
  Autor.findById(req.params.autor_id, function(err, autor) {
    if (err)
      res.send(err);
    res.json(autor);
  });
})

// update the autor with this id (PUT http://localhost:3001/autores/:autor_id)
.put(function(req, res) {
  Autor.findById(req.params.autor_id, function(err, autor) {

    if (err)
      res.send(err);
    
    autor.nome = req.body.nome;
    autor.email = req.body.email;
    autor.senha = req.body.senha;

    autor.save(function(err) {
      if (err)
        res.send(err);

      res.json({ message: 'autor updated!' });
    });
  });
})

// delete the autor with this id (DELETE http://localhost:3001/autores/:autor_id)
.delete(function(req, res) {
  Autor.remove({
    _id: req.params.autor_id
  },
  function(err, autor) {
    if (err)
        res.send(err);
      
      res.json({ message: 'Successfully deleted' });
  });
});

module.exports = router;